package com.redmango.online.sms_sender;

import android.os.Bundle;
import android.telephony.SmsManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {
    private static final String CHANNEL = "sendSms";
    private MethodChannel.Result callResult;

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        new MethodChannel(Objects.requireNonNull(getFlutterEngine()).getDartExecutor(), CHANNEL).setMethodCallHandler(
//                new MethodChannel.MethodCallHandler() {
//                    @Override
//                    public void onMethodCall(@NonNull @NotNull MethodCall call, @NonNull @NotNull MethodChannel.Result result) {
//                        if (call.method.equals("send")) {
//                            String num = call.argument("phone");
//                            String msg = call.argument("msg");
//                            sendSMS(num, msg, result);
//                        } else {
//                            result.notImplemented();
//                        }
//                    }
//                }
//        );
    }

    @Override
    public void configureFlutterEngine(@NonNull @NotNull FlutterEngine flutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);
    }

    private void sendSMS(String phoneNo, String msg, MethodChannel.Result result) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            result.success("sent");
        } catch (Exception ex) {
            ex.printStackTrace();
            result.error("Err","Sms Not Sent","");
        }
    }
}
