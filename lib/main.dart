import 'dart:async';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'app/routes/app_pages.dart';
import 'app/services/bindings/InitialBinding.dart';
import 'app/services/local_notification_service.dart';

void main() {
  runZonedGuarded(() async {
    WidgetsFlutterBinding.ensureInitialized();
    // await initAwesomeNotification();
    await GetStorage.init();
    await Firebase.initializeApp();
    FirebaseMessaging.onBackgroundMessage(backgroundHandler);
    runApp(const MyApp());
  }, (error, stack) {
    if (kDebugMode) {
      print(stack);
      print(error);
    }
  });
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: "SMS Sender",
      getPages: AppPages.routes,
      initialRoute: AppPages.INITIAL,
      initialBinding: InitialBinding(),
    );
  }
}

// Future<void> initAwesomeNotification() {
//   return AwesomeNotifications().initialize(
//   'resource://mipmap/ic_launcher',
//     [
//       NotificationChannel(
//           channelGroupKey: 'sms_sender_channel_group',
//           channelKey: 'sms_sender_channel',
//           channelName: 'SMS Sender notifications',
//           channelDescription: 'Notification channel for use',
//           defaultColor: const Color(0xFF9D50DD),
//           ledColor: Colors.white,
//       ),
//     ],
//     channelGroups: [
//       NotificationChannelGroup(
//           channelGroupkey: 'sms_sender_channel_group',
//           channelGroupName: 'SMS Sender group')
//     ],
//     debug: true,
//   );
// }

Future<void> backgroundHandler(RemoteMessage message) async {
  await GetStorage.init();
  await Firebase.initializeApp();
  LocalNotificationService.display(message);
}
