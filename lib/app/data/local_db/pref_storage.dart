import 'package:get_storage/get_storage.dart';

class PrefStorage {
  static final box = GetStorage();

  final notifications = ReadWriteValue("notifications", [], () => box);
}