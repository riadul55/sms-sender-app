class Notification {
  String? title;
  String? body;
  String? orderId;
  String? phone;
  String? message;
  String? notifyTime;
  String? sendingTime;
  bool? isSent;

  Notification({
    this.title,
    this.body,
    this.orderId,
    this.phone,
    this.message,
    this.notifyTime,
    this.sendingTime,
    this.isSent,
  });
  Notification.fromJson(Map<String, dynamic> json) {
    title = json['title']?.toString();
    body = json['body']?.toString();
    orderId = json['order_id']?.toString();
    phone = json['phone']?.toString();
    message = json['message']?.toString();
    notifyTime = json['notifyTime']?.toString();
    sendingTime = json['sendingTime']?.toString();
    isSent = json['isSent'];
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['body'] = body;
    data['order_id'] = orderId;
    data['phone'] = phone;
    data['message'] = message;
    data['notifyTime'] = notifyTime;
    data['sendingTime'] = sendingTime;
    data['isSent'] = isSent;
    return data;
  }
}