import 'package:get/get.dart';
import 'package:sms_sender/app/data/local_db/pref_storage.dart';
import 'package:sms_sender/app/services/controllers/network_controller.dart';

import '../../modules/home/controllers/home_controller.dart';
import '../controllers/FcmController.dart';

class InitialBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<PrefStorage>(PrefStorage());
    Get.put<HomeController>(HomeController());
    Get.put<NetworkController>(NetworkController());
    Get.put<FcmController>(FcmController());
  }
}