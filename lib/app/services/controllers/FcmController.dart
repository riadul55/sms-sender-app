import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sms_sender/app/modules/home/controllers/home_controller.dart';

import '../local_notification_service.dart';

class FcmController extends GetxController {
  HomeController homeController = Get.find();

  @override
  void onInit() async {
    await [
      Permission.sms,
      Permission.storage,
      Permission.notification,
      Permission.phone,
    ].request();

    if (Platform.isAndroid || Platform.isIOS) {
      await FirebaseMessaging.instance.subscribeToTopic("get-sms-info");
      print("subscribed");

      // forground get messages
      var initialMessage = await FirebaseMessaging.instance.getInitialMessage();
      if (initialMessage != null) _handleMessage(initialMessage);
      FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
        // Get.snackbar("${message.data["phone"]}", message.data['message']);
        await LocalNotificationService.display(message);
        homeController.refreshNotification();
      });
      FirebaseMessaging.onMessageOpenedApp.listen(_handleMessage);
    }
    super.onInit();
  }

  @override
  void onReady() async {
    // if (Platform.isAndroid || Platform.isIOS) {
    // }
    super.onReady();
  }

  void _handleMessage(RemoteMessage message) {
    print(message.data);
  }
}