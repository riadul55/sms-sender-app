import 'package:background_sms/background_sms.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:sms_sender/app/data/models/notfication.dart';

import '../data/local_db/pref_storage.dart';

class LocalNotificationService {
  static const String CHANNEL_KEY = "epos";
  // static const platform = MethodChannel('sendSms');

  static Future<void> display(RemoteMessage message) async {
    var orderDateTime = message.data["order_timestamp"] != null ? DateFormat("yyyy-MM-dd HH:mm:ss").parse(message.data["order_timestamp"], true).toLocal() : DateTime.now();
    var dateTimeNow = DateTime.now();
    var timeDiff = dateTimeNow.millisecondsSinceEpoch - orderDateTime.millisecondsSinceEpoch;
    var diffDateTime = DateTime.fromMillisecondsSinceEpoch(timeDiff);
    print("time diff ===> ${diffDateTime.minute}");

    var notifyData = getNotificationData(message);
    notifyData.notifyTime = "${orderDateTime.millisecondsSinceEpoch}";

    if (kDebugMode) {
      print("From firebase =====> ${notifyData.title}");
      print("From firebase =====> ${notifyData.body}");
      print("From firebase =====> ${message.data}");
    }

    var box = PrefStorage();
    List<Notification> notifications = box.notifications.val
        .map(
            (item) => item is Notification ? item : Notification.fromJson(item))
        .toList();

    if (diffDateTime.minute < 3) {

      print("SMS===> Processing");
      try {
        // final String result = await platform.invokeMethod(
        //     'send', <String, dynamic>{
        //   "phone": message.data['phone'],
        //   "msg": message.data['message']
        // });
        String phone = message.data['phone'];
        String str = phone.substring(0, 1);
        phone = str != "+" ? str != "0" ? phone = "0$phone" : phone : phone;

        SmsStatus result = await BackgroundSms.sendMessage(
            phoneNumber: phone, message: message.data['message']);
        if (result == SmsStatus.sent) {
          print("SMS===> Sent");
          notifyData.isSent = true;
          notifyData.sendingTime = "${DateTime.now().millisecondsSinceEpoch}";
          notifications.add(notifyData);
          box.notifications.val = notifications;
        } else {
          print("SMS===> Failed");
          notifyData.isSent = false;
          notifyData.sendingTime = "${DateTime.now().millisecondsSinceEpoch}";
          notifications.add(notifyData);
          box.notifications.val = notifications;
        }
      } catch (e) {
        print("SMS===> $e");
        notifyData.isSent = false;
        notifyData.sendingTime = "${DateTime.now().millisecondsSinceEpoch}";
        notifications.add(notifyData);
        box.notifications.val = notifications;
      }
    } else {
      notifyData.isSent = false;
      notifyData.sendingTime = "${DateTime.now().millisecondsSinceEpoch}";
      notifications.add(notifyData);
      box.notifications.val = notifications;
    }


    // int id = (DateTime.now().microsecondsSinceEpoch / 1000000000).round();
    // print("From firebase id =====> $id");
    //
    // AwesomeNotifications().createNotification(
    //   content: NotificationContent(
    //     id: id,
    //     channelKey: 'sms_sender_channel',
    //     title: '${message.data['phone']}',
    //     body: '${message.data['message']}',
    //     wakeUpScreen: true,
    //     autoDismissible: false,
    //   ),
    // );

    // PrefStorage storage = Get.put(PrefStorage());
  }

  static Notification getNotificationData(RemoteMessage message) {
    return Notification(
      title: message.notification!.title,
      body: message.notification!.body,
      orderId: message.data['order_id'],
      phone: message.data['phone'],
      message: message.data['message'],
    );
  }
}
