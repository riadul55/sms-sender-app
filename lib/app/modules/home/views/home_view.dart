import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:sms_sender/app/data/models/notfication.dart' as notify;

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('SMS Sender'),
        centerTitle: false,
        actions: [
          IconButton(
              onPressed: () {
                controller.refreshNotification();
              },
              icon: Icon(Icons.refresh)),
          IconButton(
              onPressed: () {
                controller.clearAllNotification();
              },
              icon: Icon(Icons.clear)),
        ],
      ),
      body: Obx(() => controller.notifyData.isNotEmpty
          ? ListView.builder(
              itemCount: controller.notifyData.length,
              itemBuilder: (context, index) {
                notify.Notification value = controller.notifyData.value[index];
                print(value.notifyTime);
                print(value.sendingTime);

                var notifyTime = DateFormat("MM/dd/yyyy, hh:mm a").format(value.notifyTime != null ? DateTime.fromMillisecondsSinceEpoch(int.parse(value.notifyTime!)) : DateTime.now());
                var sendingTime = DateFormat("MM/dd/yyyy, hh:mm a").format(value.sendingTime != null ? DateTime.fromMillisecondsSinceEpoch(int.parse(value.sendingTime!)) : DateTime.now());
                return ListTile(
                  title: Text("${value.phone}"),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Timestamp: $notifyTime"),
                      Text("SMS Sending: $sendingTime"),
                      Text("${value.message}"),
                    ],
                  ),
                  trailing: value.isSent!
                      ? const Icon(
                          Icons.radio_button_checked,
                          color: Colors.green,
                        )
                      : const Icon(
                          Icons.radio_button_unchecked,
                          color: Colors.red,
                        ),
                );
              },
            )
          : Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    height: 150,
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                      image: AssetImage("assets/images/empty_list.png"),
                    )),
                  ),
                  const SizedBox(height: 20),
                  const Text(
                    "Empty List",
                    style: TextStyle(color: Colors.black, fontSize: 16),
                  ),
                ],
              ),
            )),
      bottomNavigationBar: Container(
        height: 20,
        color: Colors.blue,
        child: Center(
            child: Obx(() => Text(
              controller.versionData.value,
              style: const TextStyle(color: Colors.white),
            ))),
      ),
    );
  }
}
