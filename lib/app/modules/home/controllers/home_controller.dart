
import 'package:get/get.dart';
import 'package:package_info/package_info.dart';
import 'package:sms_sender/app/data/local_db/pref_storage.dart';

import '../../../data/models/notfication.dart';

class HomeController extends GetxController {
  PrefStorage storage = Get.find();

  var notifyData = <Notification>[].obs;
  var versionData = "".obs;

  @override
  void onInit() async {
    super.onInit();
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    versionData(packageInfo.version);
  }

  @override
  void onReady() async {
    // requestNotifyPermission();
    super.onReady();
    refreshNotification();

  }

  @override
  void onClose() {}

  refreshNotification() {
    List<dynamic> datas = storage.notifications.val;
    print(datas.toString());
    List<Notification> notifications = datas.map((dynamic item) => item is Notification ? item : Notification.fromJson(item)).toList();
    notifyData(notifications);
  }

  clearAllNotification() async {
    await PrefStorage.box.erase();
    storage.notifications.val = [];
    notifyData([]);
  }

  // requestNotifyPermission() {
  //   AwesomeNotifications().isNotificationAllowed().then((isAllowed) {
  //     if (!isAllowed) {
  //       AwesomeNotifications().requestPermissionToSendNotifications();
  //     }
  //   });
  // }
}
